﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DropDownLabelProgram
{
    public partial class Form1 : Form
    {
        public int seconds; // Seconds. SID:1417007
        public bool paused; // State of the timer [PAUSED/WORKING].

        private int alarm = 0;
        alarm a1 = new alarm();

        int RamNumber = 0;
        public Form1()
        {
            InitializeComponent();

            lblnumber.Text = "0";
            numerator.Text = "0";
            lbldemNum.Text = "0";

            if (paused != true) //SID:1417007
            {
                if ((lblTime.Text != "")) //SID:1417007
                {
                    timer1.Enabled = true; //SID:1417007
                    try
                    {
                        seconds = System.Convert.ToInt32(lblTime.Text = "0");   //SID:1417007   
                        decimal num = numerator.Value;
                        lblnumber.Text = num.ToString();
                    }
                    catch (Exception ex) //SID:1417007
                    {
                        MessageBox.Show(ex.Message); //SID:1417007
                    }
                }
            }
            else
            {
                timer1.Enabled = true; //SID:1417007
                paused = true; //SID:1417007   
            }
        }

        private void numerator_ValueChanged(object sender, EventArgs e)
        {
            decimal num = numerator.Value;
            lbldemNum.Text = num.ToString();
            lblnumber.Text = num.ToString();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            //SID:1417007
            if (seconds == 20)
            {
                // If the time is over, clear all settings and fields.
                // Also, show the message, notifying that the time is over.
                lblTime.Text = "0";
                timer1.Enabled = false;
                bntMuteAlarm.Enabled = true;         
                
             //This section is all about button Save file as Txt file(Notepad).  SID:1417007
            // This code shows that when user press button save it will create save as file dialog. SID:1417007
            var saveFile = new SaveFileDialog();
            // This code will indicate location of the file whith a name on it as well saving file as txt.
            saveFile.Filter = "(*.txt)|*.txt";
            // To save file , program needs to have if statement which controls function of the file or method.
            // If Save file opens show windows form open file dialog ok, it means file will be saved.
            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // this code saves file as using streamwriter and using system recourses as System.IO.
                // Its saves files using name from SaveFile.Filter.  
                // This code select all the data from listbox and tryies to save items as text file converting numbers to string.
                // Saved files as one single line : Each line has one variable.
                // Also this code uses solution items to hold saved data on a program. 

                using (var sw = new System.IO.StreamWriter(saveFile.FileName, false))
                    foreach (var item in listBox1.Items)
                        sw.Write(item.ToString() + Environment.NewLine);
            }
            }

            else
            {
                // Else continue counting.
                seconds += 2;

                // Display the current values in seconds 
                // the corresponding fields.
                lblTime.Text = seconds.ToString();


                Random num = new Random();  //SID:1417007
                int[] pick1 = new int[1]; //SID:1417007

                for (int n = 0; n < pick1.Length; n++) //SID:1417007
                {
                    RamNumber = num.Next(0, 30); //SID:1417007

                    // If readings is more then 10 it will be good.
                    if (RamNumber > 10)
                    {
                        timer1.Enabled = true;
                       

                        bntBED1.Visible = true;
                        bntBED2.Visible = true;
                       
                        // Bed color.
                        bntBED1.BackColor = Color.Green;
                        bntBED2.BackColor = Color.Green;

                        grpTESTINGALARM.BackColor = Color.White;
                    }
                        // If any of the readings goes below 10 it will rise alarm.
                    else if ((RamNumber < 10))
                    {
                      //  MessageBox.Show("ALARM ALARM PATIENT IS AT CRITICAL MEDICAL STAGE ONE OF THE READING IS BAD");
                        bntBED1.Visible = true;
                        bntBED2.Visible = true;
                        grpTESTINGALARM.Visible = true;

                        // Change notification area colour.
                        grpTESTINGALARM.BackColor = Color.Yellow;

                        // Change bed colour.

                        bntBED1.BackColor = Color.Red;
                        bntBED2.BackColor = Color.Green;
  
                            System.Media.SoundPlayer s1 = new System.Media.SoundPlayer("C:/Users/JustasPC/Desktop/Uuniversiteto darbai !! 2014-2017/UNI - Year 2 -2015 - 2016/Software Engineering SEM1/DropDownLabelProgram-Justas-NEW/DropDownLabelProgram/sounds/alarm.wav");
                            s1.Play();
                            MessageBox.Show("Alarm !!! \n PATIENT IS AT CRITICAL STAGE", "Alarm!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                           alarm = 0;

                           listBox1.Items.Add("ALARM ALARM ALARM !!!! ");
                           listBox1.Items.Add("ALARM NUMBER PATIENT (VALUE)");
                           listBox1.Items.Add(lblnumber.Text);
                           listBox1.Items.Add("System timer making ramdom numbers");
                           listBox1.Items.Add(lblTime.Text);
                           listBox1.Items.Add(dateTimePicker1.Value);
                           listBox1.Items.Add("                                   ");
                    }

                    while (!(pick1.Contains(RamNumber)))  //SID:1417007
                    {
                        pick1[n] = RamNumber; //SID:1417007
                    }
                    lblnumber.Text = (RamNumber.ToString()); //SID:1417007
                }

            //    listBox1.Items.Add("Ramdom number");
              //  listBox1.Items.Add(lblnumber.Text);
              //  listBox1.Items.Add("System timer making ramdom numbers");
              //  listBox1.Items.Add(lblTime.Text);
            //    listBox1.Items.Add(dateTimePicker1.Value);
               // listBox1.Items.Add("                                   ");

                timer1.Enabled = true; //SID:1417007
                paused = false; //SID:1417007

            }

        }

        private void bntMuteAlarm_Click(object sender, EventArgs e)
        {
            System.Media.SoundPlayer s1 = new System.Media.SoundPlayer("C:/Users/JustasPC/Desktop/Uuniversiteto darbai !! 2014-2017/UNI - Year 2 -2015 - 2016/Software Engineering SEM1/DropDownLabelProgram-Justas-NEW/DropDownLabelProgram/sounds/alarm.wav");
            s1.Stop();
        }

        private void bntAlarm_Click(object sender, EventArgs e)
        {
            System.Media.SoundPlayer s1 = new System.Media.SoundPlayer("C:/Users/JustasPC/Desktop/Uuniversiteto darbai !! 2014-2017/UNI - Year 2 -2015 - 2016/Software Engineering SEM1/DropDownLabelProgram-Justas-NEW/DropDownLabelProgram/sounds/alarm.wav");
            s1.Play();
            MessageBox.Show("Alarm !!! \n PATIENT IS AT CRITICAL STAGE", "Alarm!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            alarm = 0;
        }
    }
}
