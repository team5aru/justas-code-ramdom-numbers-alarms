﻿namespace DropDownLabelProgram
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.numerator = new System.Windows.Forms.NumericUpDown();
            this.lblnumber = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbldemNum = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.bntMuteAlarm = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.grpTESTINGALARM = new System.Windows.Forms.GroupBox();
            this.bntBED2 = new System.Windows.Forms.Button();
            this.bntBED1 = new System.Windows.Forms.Button();
            this.bntRISEAlarm = new System.Windows.Forms.Button();
            this.bntAlarm = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numerator)).BeginInit();
            this.grpTESTINGALARM.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // numerator
            // 
            this.numerator.Location = new System.Drawing.Point(35, 44);
            this.numerator.Name = "numerator";
            this.numerator.Size = new System.Drawing.Size(63, 20);
            this.numerator.TabIndex = 0;
            this.numerator.ValueChanged += new System.EventHandler(this.numerator_ValueChanged);
            // 
            // lblnumber
            // 
            this.lblnumber.AutoSize = true;
            this.lblnumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnumber.Location = new System.Drawing.Point(419, 63);
            this.lblnumber.Name = "lblnumber";
            this.lblnumber.Size = new System.Drawing.Size(36, 37);
            this.lblnumber.TabIndex = 1;
            this.lblnumber.Text = "0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(132, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(292, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Number controled throught using numeric drop dowm button.";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(421, 276);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(36, 37);
            this.lblTime.TabIndex = 3;
            this.lblTime.Text = "0";
            this.lblTime.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(216, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Current label suppose to work like this.";
            // 
            // lbldemNum
            // 
            this.lbldemNum.AutoSize = true;
            this.lbldemNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldemNum.Location = new System.Drawing.Point(39, 278);
            this.lbldemNum.Name = "lbldemNum";
            this.lbldemNum.Size = new System.Drawing.Size(32, 33);
            this.lbldemNum.TabIndex = 5;
            this.lbldemNum.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(399, 253);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Current Time..";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(144, 44);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(236, 199);
            this.listBox1.TabIndex = 7;
            // 
            // bntMuteAlarm
            // 
            this.bntMuteAlarm.Location = new System.Drawing.Point(402, 126);
            this.bntMuteAlarm.Name = "bntMuteAlarm";
            this.bntMuteAlarm.Size = new System.Drawing.Size(85, 23);
            this.bntMuteAlarm.TabIndex = 8;
            this.bntMuteAlarm.Text = "Mute Alarm";
            this.bntMuteAlarm.UseVisualStyleBackColor = true;
            this.bntMuteAlarm.Click += new System.EventHandler(this.bntMuteAlarm_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(35, -1);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(130, 20);
            this.dateTimePicker1.TabIndex = 9;
            // 
            // grpTESTINGALARM
            // 
            this.grpTESTINGALARM.Controls.Add(this.bntBED2);
            this.grpTESTINGALARM.Controls.Add(this.bntBED1);
            this.grpTESTINGALARM.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTESTINGALARM.Location = new System.Drawing.Point(3, 70);
            this.grpTESTINGALARM.Name = "grpTESTINGALARM";
            this.grpTESTINGALARM.Size = new System.Drawing.Size(135, 173);
            this.grpTESTINGALARM.TabIndex = 10;
            this.grpTESTINGALARM.TabStop = false;
            this.grpTESTINGALARM.Text = "Testing Alarm";
            // 
            // bntBED2
            // 
            this.bntBED2.Location = new System.Drawing.Point(11, 110);
            this.bntBED2.Name = "bntBED2";
            this.bntBED2.Size = new System.Drawing.Size(100, 38);
            this.bntBED2.TabIndex = 1;
            this.bntBED2.Text = "Bed 2";
            this.bntBED2.UseVisualStyleBackColor = true;
            this.bntBED2.Visible = false;
            // 
            // bntBED1
            // 
            this.bntBED1.Location = new System.Drawing.Point(11, 56);
            this.bntBED1.Name = "bntBED1";
            this.bntBED1.Size = new System.Drawing.Size(100, 39);
            this.bntBED1.TabIndex = 0;
            this.bntBED1.Text = "BED 1";
            this.bntBED1.UseVisualStyleBackColor = true;
            this.bntBED1.Visible = false;
            // 
            // bntRISEAlarm
            // 
            this.bntRISEAlarm.Location = new System.Drawing.Point(402, 168);
            this.bntRISEAlarm.Name = "bntRISEAlarm";
            this.bntRISEAlarm.Size = new System.Drawing.Size(83, 23);
            this.bntRISEAlarm.TabIndex = 11;
            this.bntRISEAlarm.Text = "RISE ALARM";
            this.bntRISEAlarm.UseVisualStyleBackColor = true;
            this.bntRISEAlarm.Click += new System.EventHandler(this.timer1_Tick);
            // 
            // bntAlarm
            // 
            this.bntAlarm.Location = new System.Drawing.Point(402, 209);
            this.bntAlarm.Name = "bntAlarm";
            this.bntAlarm.Size = new System.Drawing.Size(83, 34);
            this.bntAlarm.TabIndex = 12;
            this.bntAlarm.Text = "Alarm tester";
            this.bntAlarm.UseVisualStyleBackColor = true;
            this.bntAlarm.Click += new System.EventHandler(this.bntAlarm_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 331);
            this.Controls.Add(this.bntAlarm);
            this.Controls.Add(this.bntRISEAlarm);
            this.Controls.Add(this.grpTESTINGALARM);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.bntMuteAlarm);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbldemNum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblnumber);
            this.Controls.Add(this.numerator);
            this.Name = "Form1";
            this.Text = "DropDownNumberrLabel";
            ((System.ComponentModel.ISupportInitialize)(this.numerator)).EndInit();
            this.grpTESTINGALARM.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.NumericUpDown numerator;
        private System.Windows.Forms.Label lblnumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbldemNum;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button bntMuteAlarm;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.GroupBox grpTESTINGALARM;
        private System.Windows.Forms.Button bntBED2;
        private System.Windows.Forms.Button bntBED1;
        private System.Windows.Forms.Button bntRISEAlarm;
        private System.Windows.Forms.Button bntAlarm;
    }
}

